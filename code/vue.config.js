const webpack = require("webpack");

module.exports = {
  publicPath: "/",
  configureWebpack: {
    plugins: [
      new webpack.DefinePlugin({
        "process.env": {
          PACKAGE_JSON: '"' + escape(JSON.stringify(require("./package.json"))) + '"'
        }
      })
    ]
  },
  pwa: {
    name: "Todoninja",
    themeColor: "#e2e8f0",
    workboxPluginMode: "InjectManifest",
    workboxOptions: {
      swSrc: "src/assets/service-worker.js"
    }
  }
};
