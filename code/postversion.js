const git = require('@npmcli/git')
const commit = require('libnpmversion/lib/commit.js')
const tag = require('libnpmversion/lib/tag.js')
const { version } = require('./package.json')

const opts = {
    tagVersionPrefix: 'v',
    message: 'v%s'
}

Promise.resolve().then(async () => {
    // - git add, git commit, git tag
    await git.spawn(['add', `./package.json`])
    // sometimes people .gitignore their lockfiles
    await git.spawn(['add', './package-lock.json'])
    await commit(version, opts)
    await tag(version, opts)
}).catch(console.log)