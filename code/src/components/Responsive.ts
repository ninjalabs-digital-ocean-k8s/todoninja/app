import { defineComponent } from 'vue'
import { isMobile } from '@/use/responsive'

export default defineComponent({
  render() {
    const slots = isMobile.value ? this.$slots.mobile?.(this.$attrs) : this.$slots.desktop?.(this.$attrs)
    return slots?.length == 1 ? slots[0] : slots
  }
})