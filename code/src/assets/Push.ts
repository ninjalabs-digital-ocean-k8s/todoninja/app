import PushSubscriptionModel from "@/models/PushSubscription";
import Setting from "@/models/Setting";
import { ref, Ref } from "@vue/reactivity";
import { quickModals } from "@/use/quickModals";
import { Modal } from "@/use/modal";
import PushEnsurerModal from "@/components/PushEnsurerModal.vue";

export const Push = new (class {
  static statusMessages = {
    denied: "You denied Notifications in your browser",
    unsupported: "Push is not supported on this device",
    enabled: "Push is enabled on this device",
    disabled: "Push is not enabled on this device"
  };

  public initing: Promise<void> = this.init();
  public pushManager: Ref<PushManager | null> = ref(null);
  public subscription: Ref<PushSubscription | null> = ref(null);
  public permission: Ref<PushPermissionState | null> = ref(null);

  public isDenied() {
    return this.permission.value == "denied";
  }

  public isEnabled() {
    return this.subscription.value instanceof PushSubscription;
  }

  public isSupported() {
    return this.pushManager.value instanceof PushManager;
  }

  public isAvailable() {
    return this.isSupported() && !this.isDenied();
  }

  public async init() {
    this.pushManager.value = (await navigator.serviceWorker.getRegistration())?.pushManager || null;
    this.subscription.value = (await this.pushManager.value?.getSubscription()) || null;
    this.permission.value = (await this.pushManager.value?.permissionState({ userVisibleOnly: true })) || null;
  }

  async enable() {
    await this.initing;
    if (!this.isAvailable()) {
      throw new Error("Push is not Available");
    }
    if (!this.isEnabled()) {
      const subscription = await this.pushManager.value!.subscribe({
        userVisibleOnly: true,
        applicationServerKey: "BNUy7d9SxlN1K8Grby3OTiaGooCoKJ3i7lmNrqlqSAlSFLlD6D4jFJ9a2ZypiLKSFQM62Jn1jfF3l7Oqb4GtfeA"
      });
      const p256dh = subscription.getKey("p256dh");
      const auth = subscription.getKey("auth");
      await PushSubscriptionModel.create({
        payload: JSON.stringify({
          endpoint: subscription.endpoint,
          keys: {
            p256dh: p256dh ? btoa(String.fromCharCode(...new Uint8Array(p256dh))) : null,
            auth: auth ? btoa(String.fromCharCode(...new Uint8Array(auth))) : null
          }
        })
      });
    }
    this.initing = this.init();
  }

  async disable() {
    await this.initing;
    if (this.isEnabled()) {
      await this.subscription.value!.unsubscribe();
    }
    this.initing = this.init();
  }

  async ensure(): Promise<void> {
    if ((await Setting.get("notificationHintShowed")).value) {
      // Hint already shown on this device
      return;
    }
    await this.initing;

    if (!this.isEnabled()) {
      quickModals.show(new Modal(PushEnsurerModal as any));
    }
  }
})();
