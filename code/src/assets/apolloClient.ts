import { ApolloClient, InMemoryCache } from "@apollo/client/core";
import { WebSocketLink } from "@apollo/client/link/ws";

const localUrl = new URL(window.location.href);
localUrl.protocol = "ws";
localUrl.port = "3333";
localUrl.pathname = "/graphql/";
localUrl.hash = "";

const productionUrl = new URL(`wss://${["api", ...window.location.hostname.split(".").slice(1)].join(".")}/graphql`);

export const apolloClient = new ApolloClient({
  link: new WebSocketLink({
    uri: window.location.hostname == "localhost" ? localUrl.href : productionUrl.href,
    options: {
      reconnect: true,
      connectionParams: async () => ({
        // Import dynamic bc. of circular reference in User class otherwise
        Authorization: `Bearer ${(await import("@/assets/Auth")).Auth.authKey.value}`
      })
    }
  }),
  cache: new InMemoryCache()
});
