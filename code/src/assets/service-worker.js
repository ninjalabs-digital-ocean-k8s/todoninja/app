workbox.core.setCacheNameDetails({ prefix: "todoninja-app" });

self.addEventListener("message", event => {
  if (event.data && event.data.type === "SKIP_WAITING") {
    self.skipWaiting();
  }
});

self.addEventListener("push", function(event) {
  console.log("push!");
  const data = event.data.json();
  const title = data.title;
  event.waitUntil(self.registration.showNotification(title, data));
});

self.addEventListener("notificationclick", function(event) {
  console.log("[Service Worker] Notification click Received.");

  event.waitUntil(clients.openWindow(event.notification.data.link));

  event.notification.close();
});

/**
 * The workboxSW.precacheAndRoute() method efficiently caches and responds to
 * requests for URLs in the manifest.
 * See https://goo.gl/S9QRab
 */
self.__precacheManifest = [].concat(self.__precacheManifest || []);
workbox.precaching.precacheAndRoute(self.__precacheManifest, {});
