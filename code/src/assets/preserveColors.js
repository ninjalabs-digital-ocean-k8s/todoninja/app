// Generate a list of class names to prevent them from getting purged by tailwindcss.
// Needed for classes that are used by custom workspace colors

const resolveConfig = require("tailwindcss/resolveConfig");
const tailwindConfig = require("../../tailwind.config.js");

const tailwindResolved = resolveConfig(tailwindConfig);

const generator = (prefixes, colors, variants) => {
  return prefixes
    .map(prefix => colors.map(color => variants.map(variant => `${prefix}-${color}-${variant}`)))
    .flat()
    .flat();
};

const colors = Object.keys(tailwindResolved.theme.colors);
const variants = Object.keys(tailwindResolved.theme.colors.blue);

console.log(generator(["bg", "text", "border"], colors, variants).join("\n"));
