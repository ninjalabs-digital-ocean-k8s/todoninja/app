import User from "@/models/User";
import { ref, Ref } from "@vue/reactivity";
import gql from "graphql-tag";
import localforage from "localforage";
import { apolloClient } from "./apolloClient";

export const Auth = new (class Auth {
  public authKey: Ref<string | null> = ref(null);
  public initing: Promise<void>;
  public isTokenValid: boolean = false;

  constructor() {
    this.initing = this.init();
  }

  public async user(): Promise<User | null> {
    await this.initing;
    if (this.authKey) {
      return new User();
    } else {
      return null;
    }
  }

  public async hasInvalidCredentials() {
    await this.initing;
    return this.authKey.value && !this.isTokenValid;
  }

  public async check() {
    await this.initing;
    return this.authKey.value != null && this.isTokenValid;
  }

  async init() {
    this.authKey.value = (await localforage.getItem("authKey")) as string;
    const reconnect = new Promise(resolve => (apolloClient.link as any).subscriptionClient.on("reconnected", resolve));
    (apolloClient.link as any).subscriptionClient.close(false);
    await reconnect;
    await apolloClient
      .query({
        query: gql`
          query {
            user {
              name
            }
          }
        `
      })
      .then(() => (this.isTokenValid = true))
      .catch(() => (this.isTokenValid = false));
  }

  async ensure() {
    if (!(await this.check())) {
      throw new Error("Not Authenticated!");
    }
  }

  async logout() {
    await this.initing;
    await localforage.removeItem("authKey");
  }

  async login({ username, password }: { username: string; password: string }) {
    await this.initing;
    await User.adapter.apolloClient
      .mutate({
        mutation: gql`
          mutation($email: String!, $password: String!) {
            generateToken(email: $email, password: $password) {
              token
            }
          }
        `,
        variables: { email: username, password: password }
      })
      .then(async result => {
        await localforage.setItem("authKey", result.data.generateToken.token);
        this.initing = this.init();
      });
  }
})();
