import { createApp } from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";
import Task from "./models/Task";
import Workspace from "./models/Workspace";

import Headerr from "@/components/Headerr.vue";
import Setting from "./models/Setting";
import { asyncRef } from "./models/bases/Base";
import User from "./models/User";
import Message from "./models/Message";
import { Modal } from "./use/modal/components";
import { Auth } from "./assets/Auth";
import { DateTime } from "luxon";
import PushSubscription from "./models/PushSubscription";

declare global {
  interface Window {
    Task: typeof Task;
    Workspace: typeof Workspace;
    User: typeof User;
    Setting: typeof Setting;
    Message: typeof Message;
    asyncRef: typeof asyncRef;
    Auth: typeof Auth;
    DateTime: typeof DateTime;
    PushSubscriptionModel: typeof PushSubscription;
  }
}

window.Task = Task;
window.Workspace = Workspace;
window.User = User;
window.Setting = Setting;
window.asyncRef = asyncRef;
window.Message = Message;
window.Auth = Auth;
window.DateTime = DateTime;
window.PushSubscriptionModel = PushSubscription;

export default createApp(App)
  .use(router)
  .use(store)
  .component("headerr", Headerr)!
  .component("modal", Modal)!
  .mount("#app");
