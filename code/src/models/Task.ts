import { attribute, OpaqueTableInterface, QueryBuilderContract, QueryBuilderInterface } from "@opaquejs/opaque";
import { DateTime } from "luxon";
import Workspace from "./Workspace";
import { castIntToString, date, datetime, preserveName, primaryKey } from "./extensions/Extended";
import { Base } from "./bases/Base";
import { ApolloAdapter } from "./adapters/ApolloAdapter";
import { apolloClient } from "@/assets/apolloClient";
import { SyncCopyAdapterMixin, SyncCopyAdapterTrait } from "./extensions/sync/SyncCopyAdapterMixin";
import { AdapterInterface } from "@opaquejs/opaque/lib/contracts/AdapterInterface";

export enum Status {
  Do,
  Done,
  Pending
}

export enum Procedure {
  Postponed,
  Deadlined,
  Waiting,
  Date
}

// const scope = <Model extends OpaqueTableInterface | null = null, Args extends any[] = [], QueryBuilder = Model extends OpaqueTable ? QueryBuilderContract<Model> : QueryBuilderInterface>(
//   applier: (
//     query: QueryBuilder,
//     ...args: Args
//   ) => QueryBuilder
// ) => (...args: Args) => (query: QueryBuilder) =>
//   applier(query, ...args);
type QueryScopeInterface = (...args: any[]) => (query: QueryBuilderInterface) => QueryBuilderInterface;
const scopeFor = <Model extends OpaqueTableInterface>() => <Args extends any[], Query = QueryBuilderContract<Model>>(
  applier: (query: Query, ...args: Args) => Query
) => (...args: Args) => (query: Query) => applier(query, ...args);
const scope = <Args extends any[], Query extends QueryBuilderInterface = QueryBuilderInterface>(
  applier: (query: Query, ...args: Args) => Query
) => (...args: Args) => (query: Query) => applier(query, ...args);

const statusScopes = {
  done: scopeFor<typeof Task>()((query, now: DateTime = DateTime.now()) => query.where("doneAt", "<", now)),
  undone: scopeFor<typeof Task>()((query, now: DateTime = DateTime.now()) =>
    query.where("doneAt", ">", now).orWhere("doneAt", null)
  ),
  pendingRaw: scopeFor<typeof Task>()((query, now: DateTime = DateTime.now()) =>
    query
      .where("postponedUntil", ">", now.endOf("day"))
      .orWhere(query => query.where("waitingFor", "!=", null).andWhere("postponedUntil", null))
  ),
  pending: scopeFor<typeof Task>()((query, now: DateTime = DateTime.now()) =>
    query.apply("undone", now).apply("pendingRaw", now)
  ),
  do: scopeFor<typeof Task>()((query, now: DateTime = DateTime.now()) =>
    query.apply("undone", now).andWhere(query =>
      // Select not pending
      query
        .where(query =>
          // Select unpostponed and former postponed
          query.where("postponedUntil", "<", now.endOf("day")).orWhere("postponedUntil", null)
        )
        .andWhere(query =>
          // Select former waiting
          query.where("waitingFor", null).orWhere("postponedUntil", "<", now.endOf("day"))
        )
    )
  )
};

@preserveName("Task")
export default class Task extends Base {
  static adapter: AdapterInterface & SyncCopyAdapterTrait = new (SyncCopyAdapterMixin(ApolloAdapter))(
    Task,
    apolloClient
  );

  static scopes: Record<string, QueryScopeInterface> = statusScopes as any;

  @castIntToString()
  @primaryKey({
    serialize: (value: unknown) => (value == "new" ? undefined : value)
  })
  public id?: string = undefined;

  @attribute({
    get: (value: string) => (value == undefined ? "" : value)
  })
  public title: string = "";

  @datetime()
  public doneAt: DateTime | null = null;

  @attribute({
    get: (value: string) => (value == undefined ? "" : value)
  })
  public description: string = "";

  @date()
  public todayAt: DateTime | null = null;

  @date()
  public postponedUntil: DateTime | null = null;

  @date()
  public deadlineAt: DateTime | null = null;

  @datetime()
  public remindAt: DateTime | null = null;

  @attribute()
  public waitingFor: string | null = null;

  @castIntToString()
  @attribute()
  public workspaceId: string | null = null;

  workspace() {
    return this.belongsTo(Workspace).withDefault(() => Workspace.inbox);
  }

  // For fuzzy search
  get workspaceSync(): Workspace | undefined {
    return this.workspace().execSync();
  }

  isDone(now: DateTime = DateTime.local()) {
    return this.status(now) == Status.Done;
  }
  isDo(now: DateTime = DateTime.local()) {
    return this.status(now) == Status.Do;
  }
  isPending(now: DateTime = DateTime.local()) {
    return this.status(now) == Status.Pending;
  }

  isOverdue(now: DateTime = DateTime.local()) {
    return !this.isDone(now) && this.isDeadlineOverdue(now);
  }
  isToday(now: DateTime = DateTime.local()) {
    return this.isDeadlineToday(now);
  }
  isTomorrow(now: DateTime = DateTime.local()) {
    return this.isDeadlineToday(now.plus({ days: 1 }));
  }
  overdueSince(now: DateTime = DateTime.local()) {
    return this.deadlineOverdueSince(now);
  }

  isFocus() {
    return this.todayAt instanceof DateTime;
  }
  isDeadlineToday(now: DateTime = DateTime.local()) {
    return this.deadlineAt instanceof DateTime && this.deadlineAt.hasSame(now, "day");
  }
  isDeadlineOverdue(now: DateTime = DateTime.local()) {
    return (this.deadlineOverdueSince(now)?.as("days")! || -Infinity) > 0;
  }
  deadlineOverdueSince(now: DateTime = DateTime.local()) {
    return this.deadlineAt instanceof DateTime
      ? now.startOf("day").diff(this.deadlineAt.startOf("day"), "days")
      : undefined;
  }

  get isDate() {
    return this.procedures.includes(Procedure.Date);
  }
  get isPostponed() {
    return this.procedures.includes(Procedure.Postponed);
  }
  get isWaiting() {
    return this.procedures.includes(Procedure.Waiting);
  }
  get isDeadlined() {
    return this.procedures.includes(Procedure.Deadlined);
  }

  toggleDone(this: Task) {
    this.doneAt = this.isDone() ? null : DateTime.fromObject({});
  }

  // Is actually Focus
  toggleToday(this: Task): Task;
  toggleToday(this: Task, save: true): Promise<Task>;
  toggleToday(this: Task, save: boolean = false): any {
    if (!this.isFocus()) {
      this.todayAt = DateTime.local();
    } else {
      this.todayAt = null;
    }
    if (save && this.$isPersistent) {
      return this.$saveOnly(["todayAt"]);
    } else {
      return Task;
    }
  }

  // Procedures are reasons why a task has a certiain status
  get procedures() {
    const procedures = [];
    if (this.postponedUntil && this.deadlineAt && this.postponedUntil.equals(this.deadlineAt)) {
      procedures.push(Procedure.Date);
    } else {
      if (this.postponedUntil) {
        procedures.push(Procedure.Postponed);
      }
      if (this.deadlineAt) {
        procedures.push(Procedure.Deadlined);
      }
    }
    if (this.waitingFor) {
      procedures.push(Procedure.Waiting);
    }
    return procedures;
  }

  status(now: DateTime = DateTime.local()) {
    if (
      Task.adapter.localCopy.engine.matchesQuery(
        this.$serialize(),
        statusScopes
          .done(now)(Task.query())
          .$getQuery()
      )
    ) {
      return Status.Done;
    }
    if (
      Task.adapter.localCopy.engine.matchesQuery(
        this.$serialize(),
        statusScopes
          .pending(now)(Task.query())
          .$getQuery()
      )
    ) {
      return Status.Pending;
    }
    return Status.Do;
  }

  static groupByWorkspace(tasks: Task[]) {
    return tasks.reduce((grouped, task) => {
      let group = grouped.find(group => group[0]?.workspaceId == task.workspaceId);
      if (!group) {
        group = [];
        grouped.push(group);
      }
      group.push(task);
      return grouped;
    }, [] as Task[][]);
  }
}
