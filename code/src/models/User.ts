import { attribute } from "@opaquejs/opaque";
import { ApolloAdapter } from "./adapters/ApolloAdapter";
import { Base } from "./bases/Base";
import { preserveName, primaryKey } from "./extensions/Extended";
import { ComputedRef } from "@vue/reactivity";
import { computed } from "@vue/runtime-core";
import Setting from "./Setting";
import { apolloClient } from "@/assets/apolloClient";

@preserveName("User")
export default class User extends Base {
  static adapter: ApolloAdapter = new ApolloAdapter(User, apolloClient, {
    keys: (method?: "read") =>
      Array.from(User.$schema.keys()).filter(key => (method == "read" ? key != "password" : true))
  });

  @primaryKey()
  public id?: string = undefined;

  @attribute()
  public name: string = "";

  @attribute()
  public email: string = "";

  @attribute()
  public password: string = "";

  public static authKey: ComputedRef<string | undefined> = computed(() => Setting.getSync("authKey").value);

  // static get apolloOptions() {
  //   return {
  //     ...super.apolloOptions,
  //     keys: () => [...this.$schema.keys()].filter(key => key != "password")
  //   };
  // }
}
