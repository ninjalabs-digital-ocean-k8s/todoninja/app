import { attribute } from "@opaquejs/opaque";
import { ModelAttributes } from "@opaquejs/opaque/lib/contracts/ModelContracts";
import { GraphQLError } from "graphql";
import { LocalAdapter } from "./adapters/LocalStorageAdapter";
import { Base } from "./bases/Base";
import { preserveName } from "./extensions/Extended";

@preserveName("Message")
export default class Message extends Base {
  static adapter: LocalAdapter = new LocalAdapter(Message);

  @attribute({ primaryKey: true })
  public id?: string = undefined;

  @attribute()
  public title: string = "";

  @attribute()
  public body: string = "";

  @attribute()
  public color: string = "";

  @attribute()
  public icon: string = "";

  @attribute()
  public actions?: { name: string; click: () => any }[] = undefined;

  static error(data: Partial<ModelAttributes<Message>>) {
    return this.make({ color: "red", title: "Error", ...data });
  }

  static fromGraphQlError(error: GraphQLError) {
    if (error.message.startsWith("E_VALIDATION_FAILURE:")) {
      const messages = Object.entries(error.extensions?.exception?.messages || {}) as [string, string[]][];
      return this.error({
        title: "Invalid Input",
        body: messages.map(([field, errors]) => `${field}: ${errors.join(", ")}`).join(", ")
      });
    }
    return this.error({ title: error.message });
  }
}
