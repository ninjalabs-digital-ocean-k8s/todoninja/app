import { OpaqueAttributes, OpaqueTableInterface, PrimaryKeyValue } from "@opaquejs/opaque/lib/contracts/ModelContracts";
import { NormalizedQuery } from "@opaquejs/query";
import { QueryEngine } from "@opaquejs/query-engine";
import { Comparator } from "@opaquejs/query-engine/lib/Comparator";
import { Comparators } from "@opaquejs/query-engine/lib/contracts/Comparator";
import { reactive } from "@vue/reactivity";
import { DateTime } from "luxon";
import { AdapterInterface } from "@opaquejs/opaque";
import { Constructor } from "@opaquejs/opaque/lib/util";
import Observable from "zen-observable";
import { SyncAdapterInterfaceMixin } from "./interfaces";

const tap = <T>(something: T) => (callback: (something: T) => unknown) => {
  callback(something);
  return something;
};

class DateTimeComparator extends Comparator {
  parseDate(value: unknown) {
    if (DateTime.isDateTime(value)) {
      return value;
    }
    if (this.isEmpty(value)) {
      return value;
    }
    if (typeof value != "string") {
      throw new Error(
        `A date value must be given as a string, received value [${value}] with type [${typeof value}] in date comparison`
      );
    }
    return DateTime.fromISO(value);
  }

  compare(left: unknown, comparison: Comparators, right: unknown) {
    return super.compare(this.parseDate(left), comparison, this.parseDate(right));
  }

  public "=="(left: DateTime, right: DateTime) {
    return left.equals(right);
  }
}

class Collection extends Map<PrimaryKeyValue, OpaqueAttributes> {
  public engine = new QueryEngine({
    comparators: {
      postponedUntil: ctx => new DateTimeComparator(ctx),
      deadlineAt: ctx => new DateTimeComparator(ctx),
      doneAt: ctx => new DateTimeComparator(ctx),
      todayAt: ctx => new DateTimeComparator(ctx)
    },
    mode: "sql"
  });

  constructor(public model: OpaqueTableInterface) {
    super();
  }
  insertOrUpdate(items: OpaqueAttributes[]) {
    for (const item of items) {
      const pk = item[this.model.primaryKey] as PrimaryKeyValue;
      if (!this.has(pk)) this.set(pk, {});
      Object.assign(this.get(pk), item);
    }
  }

  query(query: NormalizedQuery) {
    return this.engine.queryCollection(Array.from(this.values()), query);
  }
}

export interface SyncCopyAdapterTrait extends SyncAdapterInterfaceMixin<NormalizedQuery> {
  localCopy: Collection;
  readSync(query: NormalizedQuery): OpaqueAttributes[];
  insertSync(data: OpaqueAttributes): PrimaryKeyValue | OpaqueAttributes;
}

export interface RealtimeAdapterInterface<Query> {
  insertedSubscription(): Observable<OpaqueAttributes>;
  updatedSubscription(): Observable<OpaqueAttributes>;
  deletedSubscription(): Observable<PrimaryKeyValue>;
}

export const SyncCopyAdapterMixin = <
  T extends Constructor<
    AdapterInterface<NormalizedQuery> & RealtimeAdapterInterface<NormalizedQuery> & { model: OpaqueTableInterface }
  >
>(
  base: T
) =>
  class SyncAdapterMixin extends base implements SyncCopyAdapterTrait {
    public localCopy: Collection = reactive(new Collection(this.model));

    async read(query: NormalizedQuery) {
      return tap(await super.read(query))(items => this.localCopy.insertOrUpdate(items));
    }
    async insert(data: OpaqueAttributes) {
      return tap(await super.insert(data))(result => {
        if (typeof result == "object") {
          this.localCopy.insertOrUpdate([result]);
        } else {
          this.localCopy.insertOrUpdate([{ ...data, [this.model.primaryKey]: result }]);
        }
      });
    }
    async update(query: NormalizedQuery, data: OpaqueAttributes) {
      return tap(await super.update(query, data))(() => {
        this.localCopy.insertOrUpdate(this.localCopy.query(query).map(item => ({ ...item, ...data })));
      });
    }
    async delete(query: NormalizedQuery) {
      return tap(await super.delete(query))(() => {
        this.localCopy
          .query(query)
          .forEach(item => this.localCopy.delete(item[this.model.primaryKey] as PrimaryKeyValue));
      });
    }

    readSync(query: NormalizedQuery) {
      return this.localCopy.query(query);
    }
    insertSync(data: OpaqueAttributes) {
      this.localCopy.insertOrUpdate([data]);
      return data;
    }

    realtimeSync() {
      this.insertedSubscription().forEach(item => this.localCopy.insertOrUpdate([item]));
      this.updatedSubscription().forEach(item => this.localCopy.insertOrUpdate([item]));
      this.deletedSubscription().forEach(item => this.localCopy.delete(item));
    }
  };
