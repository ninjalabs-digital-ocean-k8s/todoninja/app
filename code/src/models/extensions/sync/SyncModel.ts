import { QueryBuilder, QueryBuilderContract, QueryBuilderInterface } from "@opaquejs/opaque";
import {
  BelongsToRelationContract,
  BelongsToRelationInterface,
  HasManyRelationContract,
  HasManyRelationInterface,
  OpaqueRow,
  OpaqueTable,
  OpaqueTableInterface,
  PrimaryKeyValue
} from "@opaquejs/opaque/lib/contracts/ModelContracts";
import { Constructor } from "@opaquejs/opaque/lib/util";
import { SyncQueryBuilderTrait } from "./interfaces";

type ReplaceOmit<Target, Source> = Omit<Target, keyof Source> & Source;
type Replace<Target, Source> = Source & Target;

export type OldTrait<Target extends Constructor, Static, Instance> = Replace<Target, Static> &
  Constructor<Replace<InstanceType<Target>, Instance>>;
export type Trait<Target extends Constructor, Static extends Constructor> = Replace<Target, Static> &
  Constructor<Replace<InstanceType<Target>, InstanceType<Static>>>;

interface SyncTrait {
  belongsTo<Local extends OpaqueRow, Foreign extends OpaqueTable>(
    this: Local,
    foreign: Foreign
  ): Replace<
    BelongsToRelationContract<Local, Foreign>,
    { query(): QueryBuilderContract<Foreign> & SyncQueryBuilderTrait<Foreign>; execSync(): InstanceType<Foreign> }
  >;
  hasMany<Foreign extends OpaqueTable>(
    foreign: Foreign
  ): Replace<
    HasManyRelationContract<Foreign>,
    { query(): QueryBuilderContract<Foreign> & SyncQueryBuilderTrait<Foreign>; execSync(): InstanceType<Foreign>[] }
  >;
}

interface SyncStaticTrait {
  new (): SyncTrait;
  query<This extends OpaqueTable>(this: This): QueryBuilder<This> & SyncQueryBuilderTrait<This>;
  findSync<This extends OpaqueTable>(this: This, key: PrimaryKeyValue): InstanceType<This> | undefined;
  allSync<This extends OpaqueTable>(this: This): InstanceType<This>[];
}

const syncQueryBuilder = <T extends Constructor<QueryBuilderInterface>>(base: T) =>
  class extends base {
    getSync(this: any) {
      return this.$hydrate(this.model.adapter.readSync(this.$getQuery()));
    }
    firstSync(this: any) {
      return this.limit(1).getSync()[0];
    }
  };
const syncBelongsToRelation = <T extends Constructor<BelongsToRelationInterface>>(base: T) =>
  class extends base {
    execSync(this: any) {
      return this.query().firstSync() || this.default();
    }
  };
const syncHasManyRelation = <T extends Constructor<HasManyRelationInterface>>(base: T) =>
  class extends base {
    execSync(this: any) {
      return this.query().getSync();
    }
  };

export const syncModel = <T extends OpaqueTableInterface>(t: T): Trait<T, SyncStaticTrait> => {
  // @ts-ignore
  class SyncModel extends t {
    $BelongsToRelationConstructor() {
      return syncBelongsToRelation(super.$BelongsToRelationConstructor());
    }
    $HasManyRelationConstructor() {
      return syncHasManyRelation(super.$HasManyRelationConstructor());
    }
    static $QueryConstructor() {
      return syncQueryBuilder(super.$QueryConstructor());
    }
    static findSync(this: any, key: PrimaryKeyValue) {
      return this.query()
        .for(key)
        .firstSync();
    }

    static allSync(this: any) {
      return this.query().getSync();
    }
  }
  return SyncModel as any;
};
