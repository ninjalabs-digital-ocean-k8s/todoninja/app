import {
  OpaqueAttributes,
  OpaqueRow,
  OpaqueRowInterface,
  OpaqueTable
} from "@opaquejs/opaque/lib/contracts/ModelContracts";

export interface SyncAdapterInterfaceMixin<T> {
  readSync: (query: T) => OpaqueAttributes[];
}

export interface SyncQueryBuilderInterfaceMixin {
  getSync(): OpaqueRowInterface[];
  firstSync(): OpaqueRowInterface;
}
export interface SyncQueryBuilderTrait<Model extends OpaqueTable> extends SyncQueryBuilderInterfaceMixin {
  getSync(): InstanceType<Model>[];
  firstSync(): InstanceType<Model>;
}
