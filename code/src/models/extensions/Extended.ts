import { attribute, OpaqueModel } from "@opaquejs/opaque";
import { AttributeOptionsContract, OpaqueRow, OpaqueTable } from "@opaquejs/opaque/lib/contracts/ModelContracts";
import { DateTime } from "luxon";

export const datetime = <Type>(options: Partial<AttributeOptionsContract<Type>> = {}) =>
  attribute({
    deserialize: ((value: string) => (value ? DateTime.fromISO(value) : value)) as any,
    serialize: ((value: DateTime | null) => (value instanceof DateTime ? value.toISO() : value)) as any
  });
export const date = <Type>(options: Partial<AttributeOptionsContract<Type>> = {}) =>
  attribute({
    get: ((value: string) => (value ? DateTime.fromISO(value) : value)) as any,
    set: ((value: DateTime | null) => (value instanceof DateTime ? value.toISODate() : value)) as any
  });

export const primaryKey = <Type>(options: Partial<AttributeOptionsContract<Type> & { default: never }> = {}) =>
  attribute({
    primaryKey: true,
    ...options
  });

export const castIntToString = () => <M extends OpaqueRow>(model: M, property: string) => {
  const attribute = (model.constructor as OpaqueTable).$schema.get(property);
  if (!attribute) {
    throw new Error("attribute is not defined, and thus cannot be casted");
  }
  const previousSerialize = attribute.serialize;
  const previouDeserialize = attribute.deserialize;
  attribute.serialize = (value: unknown) => {
    value = previousSerialize(value);
    if (value == undefined) {
      return value;
    }
    const parsed = parseInt(value as string);
    if (Number.isNaN(parsed)) {
      throw new Error(
        `[${value}] could not be casted back to an integer on model [${model.constructor.name}] and attribute [${property}].`
      );
    }
    return parsed;
  };
  attribute.deserialize = (value: unknown) => {
    return value == undefined ? value : `${previouDeserialize(value)}`;
  };
};

export const preserveName = (name: string) => (constructor: typeof OpaqueModel) =>
  Object.defineProperty(constructor, "name", { value: name });
