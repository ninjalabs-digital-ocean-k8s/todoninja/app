import { OpaqueModel } from "@opaquejs/opaque";
import { ModelAttributes } from "@opaquejs/opaque/lib/contracts/ModelContracts";
import { Ref, ref, UnwrapRef, watchEffect } from "vue";
import { syncModel } from "../extensions/sync/SyncModel";
import { vueModel } from "../extensions/VueModel";

export class Base extends syncModel(vueModel(OpaqueModel)) {
  public async $setAndSaveAttributes(
    attributes: Partial<ModelAttributes<this>>,
    { create = false }: { create?: boolean } = {}
  ) {
    if (!this.$isPersistent && create == false) {
      return this.$setAttributes(attributes);
    } else {
      return await super.$setAndSaveAttributes(attributes);
    }
  }
}

const test = syncModel(class extends OpaqueModel {});

export function asyncRef<Value>(value: (() => Promise<Value>) | Promise<Value>): Ref<UnwrapRef<Value | undefined>>;
export function asyncRef<Value>(
  value: (() => Promise<Value>) | Promise<Value>,
  defaultValue: Value
): Ref<UnwrapRef<Value>>;
export function asyncRef<Value, Default extends Value | undefined>(
  value: (() => Promise<Value>) | Promise<Value>,
  defaultValue?: Default
) {
  const model = defaultValue ? ref<Value>(defaultValue as Value) : ref<Value>();
  if (value instanceof Function) {
    watchEffect(() => value().then(m => (model.value = m)));
  } else {
    value.then(m => (model.value = m));
  }
  return model;
}
