import { attribute } from "@opaquejs/opaque";
import { preserveName } from "./extensions/Extended";
import { Base } from "./bases/Base";
import { ApolloAdapter } from "./adapters/ApolloAdapter";
import { apolloClient } from "@/assets/apolloClient";
import { AdapterInterface } from "@opaquejs/opaque/lib/contracts/AdapterInterface";

@preserveName("PushSubscription")
export default class PushSubscription extends Base {
  static adapter: AdapterInterface = new ApolloAdapter(PushSubscription, apolloClient);

  @attribute()
  public payload: string = "";
}
