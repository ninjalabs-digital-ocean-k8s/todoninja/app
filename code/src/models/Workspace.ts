import { attribute } from "@opaquejs/opaque";
import Task from "./Task";
import { castIntToString, preserveName, primaryKey } from "./extensions/Extended";
import { Base } from "./bases/Base";
import { ApolloAdapter } from "./adapters/ApolloAdapter";
import { apolloClient } from "@/assets/apolloClient";
import { SyncCopyAdapterMixin } from "./extensions/sync/SyncCopyAdapterMixin";
import { DateTime } from "luxon";
import { AdapterInterface } from "@opaquejs/opaque/lib/contracts/AdapterInterface";

@preserveName("Workspace")
export default class Workspace extends Base {
  static adapter: AdapterInterface = new (SyncCopyAdapterMixin(ApolloAdapter))(Workspace, apolloClient);

  @castIntToString()
  @primaryKey({
    serialize: id => (id === null ? undefined : id),
    deserialize: id => (id === undefined ? null : id)
  })
  public id: string | null = null;

  @attribute()
  public name: string = "";

  @attribute({
    get: (value: string) => ([...Workspace.COLORS, "white"].includes(value) ? value : "blue")
  })
  public color?: string = undefined;

  @attribute()
  public icon?: string = undefined;

  public isInbox: boolean = false;

  public static inbox: Workspace;

  public static COLORS = ["blue", "red", "orange", "yellow", "green", "gray", "teal", "indigo", "purple", "pink"];

  tasks() {
    return this.hasMany(Task);
  }
}

Workspace.inbox = new Workspace();
Workspace.inbox.isInbox = true;
Workspace.inbox.$setAttributes({
  name: "Inbox",
  color: "white",
  icon: "inbox"
});
