import { attribute } from "@opaquejs/opaque";
import { RouteLocationRaw } from "vue-router";
import { LocalAdapter, LocalStorageAdapter } from "./adapters/LocalStorageAdapter";
import { Base } from "./bases/Base";
import { preserveName } from "./extensions/Extended";

type Defaults = {
  lastView?: RouteLocationRaw;
  authKey?: string;
  notificationHintShowed?: true;
};

const defaults: Defaults = {};

@preserveName("Setting")
export default class Setting<T = string> extends Base {
  static adapter: LocalAdapter = new LocalStorageAdapter(Setting, { manualId: true });

  @attribute({ primaryKey: true })
  public key?: string = undefined;

  @attribute()
  public value?: T = undefined;

  public static async get<B extends keyof typeof defaults>(name: B) {
    const newSetting = new this<typeof defaults[B] | undefined>();
    newSetting.key = name;
    newSetting.value = defaults[name];
    return ((await this.find(name)) || newSetting) as typeof newSetting;
  }
  public static getSync<B extends keyof typeof defaults>(name: B) {
    const newSetting = new this<typeof defaults[B] | undefined>();
    newSetting.key = name;
    newSetting.value = defaults[name];
    return (this.findSync(name) || newSetting) as typeof newSetting;
  }

  public static async set<T extends keyof typeof defaults>(name: T, value?: typeof defaults[T]) {
    const setting = (await this.findOrCreate(name)) as Setting<typeof defaults[T]>;
    setting.key = name;
    setting.value = value;
    return await setting.save();
  }
}
