import { queryCollection } from "@opaquejs/query-engine";
import { NormalizedQuery } from "@opaquejs/query";
import localforage from "localforage";
import { OpaqueAttributes, OpaqueTable, PrimaryKeyValue } from "@opaquejs/opaque/lib/contracts/ModelContracts";
import { reactive } from "@vue/reactivity";
import { SyncAdapterInterfaceMixin } from "../extensions/sync/interfaces";
import { AdapterInterface } from "@opaquejs/opaque/lib/contracts/AdapterInterface";

export type LocalAdapterOptions = {
  manualId: boolean;
};

export class LocalAdapter implements AdapterInterface, SyncAdapterInterfaceMixin<NormalizedQuery> {
  storage: Map<PrimaryKeyValue, OpaqueAttributes> = reactive(new Map());
  public id: number = 1;

  constructor(
    public model: { primaryKey: PrimaryKeyValue; name: string },
    public options: LocalAdapterOptions = { manualId: false }
  ) {}

  async delete(query: NormalizedQuery) {
    return this.deleteSync(query);
  }
  async update(query: NormalizedQuery, data: OpaqueAttributes) {
    return this.updateSync(query, data);
  }
  async read(query: NormalizedQuery) {
    return this.readSync(query);
  }
  async insert(data: OpaqueAttributes) {
    return this.insertSync(data);
  }

  deleteSync(query: NormalizedQuery) {
    for (const { [this.model.primaryKey]: id } of queryCollection([...this.storage.values()], query)) {
      this.storage.delete(id as PrimaryKeyValue);
    }
  }

  insertSync(data: OpaqueAttributes) {
    const obj = { ...data } as any;
    if (!this.options.manualId) {
      obj[this.model.primaryKey] = Math.floor(Math.random() * 1000000);
    }
    this.storage.set(obj[this.model.primaryKey], obj);
    return this.storage.get(obj[this.model.primaryKey])!;
  }

  updateSync(query: NormalizedQuery, data: OpaqueAttributes) {
    for (const { [this.model.primaryKey]: id } of this.readSync(query)) {
      const previous = this.storage.get(id as PrimaryKeyValue)!;
      for (const key in data) {
        previous[key] = data[key];
      }
    }
  }

  readSync(query: NormalizedQuery) {
    return queryCollection([...this.storage.values()], query);
  }
}

export class LocalStorageAdapter extends LocalAdapter {
  public $fetched: Promise<void>;

  constructor(public model: OpaqueTable, public options: LocalAdapterOptions = { manualId: false }) {
    super(model);
    this.$fetched = this.fetch();
  }

  async delete(id: NormalizedQuery) {
    await super.delete(id);
    this.saveEntriesToLocalstorage();
  }

  async insert(data: OpaqueAttributes) {
    const id = await super.insert(data);
    this.saveEntriesToLocalstorage();
    return id;
  }

  async update(id: NormalizedQuery, data: OpaqueAttributes) {
    const result = await super.update(id, data);
    this.saveEntriesToLocalstorage();
    return result;
  }

  async read(query: NormalizedQuery) {
    await this.$fetched;
    return super.read(query);
  }

  async fetch() {
    this.storage.clear();
    for (const [id, data] of await this.getEntriesFromLocalstorage()) {
      this.storage.set(id, data);
    }
  }

  public async getEntriesFromLocalstorage() {
    const r = (await localforage.getItem(this.model.name)) as string;
    return (JSON.parse(r || "[]") as [PrimaryKeyValue, OpaqueAttributes][]) || [];
  }

  public async saveEntriesToLocalstorage(): Promise<void> {
    await this.$fetched;
    await localforage.setItem(this.model.name, JSON.stringify(Array.from(this.storage.entries())));
  }
}
