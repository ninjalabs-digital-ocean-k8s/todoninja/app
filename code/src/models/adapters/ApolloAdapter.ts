import { camelize, pascalize } from "@/inflector";
import { ApolloClient, gql } from "@apollo/client/core";
import { NormalizedQuery } from "@opaquejs/query";
import inflector from "inflector-js";
import { AdapterInterface } from "@opaquejs/opaque/lib/contracts/AdapterInterface";
import { OpaqueAttributes, OpaqueTableInterface } from "@opaquejs/opaque/lib/contracts/ModelContracts";
import { Ref, ref } from "vue";

export type GraphQlConfig = {
  keys?: () => string[];
};

export type ParsedGraphQlConfig = GraphQlConfig & {
  keys: (method?: "read") => string[];
};

export type GraphQlable = {
  graphQlConfig?: GraphQlConfig;
};

export class ApolloAdapter implements AdapterInterface {
  public options: ParsedGraphQlConfig;

  static loading: Ref<number> = ref(0);

  constructor(public model: OpaqueTableInterface, public apolloClient: ApolloClient<any>, options?: GraphQlConfig) {
    this.options = {
      keys: options?.keys || (() => [...this.model.$schema.keys()])
    };
  }

  static async load<T>(loader: () => Promise<T>): Promise<T> {
    this.loading.value++;
    try {
      const result = await loader();
      return result;
    } catch (e) {
      throw e;
    } finally {
      this.loading.value--;
    }
  }
  async load<T>(loader: () => Promise<T>): Promise<T> {
    return (this.constructor as typeof ApolloAdapter).load(loader);
  }

  async delete(query: NormalizedQuery) {
    await this.load(() =>
      this.apolloClient.mutate({
        mutation: gql`mutation ($query: JsonOpaqueQuery!) { delete${pascalize(
          this.model.name
        )}(query: $query) { affectedRows } }`,
        variables: { query }
      })
    );
  }

  insert(data: OpaqueAttributes) {
    return this.load(() => {
      return this.apolloClient.mutate({
        mutation: gql`mutation ($model: ${pascalize(this.model.name)}CreateInput!) { result: create${pascalize(
          this.model.name
        )}(${camelize(this.model.name)}: $model) { ${this.options.keys("read").join(" ")} } }`,
        variables: { model: data }
      });
    }).then(r => r.data.result as OpaqueAttributes);
  }

  async update(query: NormalizedQuery, data: OpaqueAttributes) {
    await this.load(() =>
      this.apolloClient.mutate({
        mutation: gql`mutation ($model: ${pascalize(
          this.model.name
        )}PatchInput!, $query: JsonOpaqueQuery) { result: patch${pascalize(this.model.name)}(${camelize(
          this.model.name
        )}: $model, query: $query) { ${this.options.keys("read").join(" ")} } }`,
        variables: { model: data, query }
      })
    );
  }

  read(query: OpaqueAttributes) {
    return this.load(() =>
      this.apolloClient.query({
        query: gql`query ($query: JsonOpaqueQuery!) { result: ${inflector.pluralize(
          camelize(this.model.name)
        )}(query: $query) { ${this.options.keys("read").join(" ")} } }`,
        variables: { query }
      })
    ).then(r => r.data.result as OpaqueAttributes[]);
  }

  insertedSubscription() {
    return this.apolloClient
      .subscribe({
        query: gql`subscription { result: ${camelize(this.model.name)}Created { ${this.options.keys().join(",")} } }`
      })
      .map(item => item.data.result as OpaqueAttributes);
  }

  updatedSubscription() {
    return this.apolloClient
      .subscribe({
        query: gql`subscription { result: ${camelize(this.model.name)}Updated { ${this.options.keys().join(",")} } }`
      })
      .map(item => item.data.result);
  }

  deletedSubscription() {
    return this.apolloClient
      .subscribe({
        query: gql`subscription { result: ${camelize(this.model.name)}Deleted { ${this.options.keys().join(",")} } }`
      })
      .map(item => item.data.result[this.model.primaryKey]);
  }
}
