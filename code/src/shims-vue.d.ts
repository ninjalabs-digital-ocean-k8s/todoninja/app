declare module "*.vue" {
  import Vue from "vue";
  export default Vue;
}
declare module "*.svg" {
  const path: string;
  export default path;
}

declare module "*/tailwind.resolved" {
  const val: any;
  export default val;
}

declare module "inflector-js" {
  const val: any;
  export default val;
}

declare module "graphql.js" {
  const val: any;
  export default val;
}

declare module "tailwindcss/resolveConfig" {
  const val: any;
  export default val;
}

declare module "@/../tailwind.config.js" {
  const val: any;
  export default val;
}
