import resolveConfig from 'tailwindcss/resolveConfig';
import tailwindConfig from '@/../tailwind.config.js';

const tailwindResolved = resolveConfig(tailwindConfig);

export default tailwindResolved;
