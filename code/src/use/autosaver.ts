import { Base } from "@/models/bases/Base";
import { onMounted, onUnmounted, watch, WatchSource, WatchStopHandle } from "@vue/runtime-core";
import { debounce } from "./debounce";
import { Loader } from "./Loader";

export class Autosaver extends EventTarget {
  public loader = new Loader();
  public timeout: number;

  protected modelWatchStopHandle: WatchStopHandle | null = null;
  protected dirtyWatchStopHandle: WatchStopHandle | null = null;

  constructor(public model: WatchSource<Base>, { timeout = 0 }: { timeout?: number } = {}) {
    super();
    this.timeout = timeout;
  }

  start() {
    this.modelWatchStopHandle = watch(
      this.model,
      model => {
        const debouncedSave = debounce(() => {
          if (model.$isDirty && model.$isPersistent) {
            this.loader.load(model.save()).then(() => this.dispatchEvent(new Event("saved")));
          }
        }, 1000);
        this.dirtyWatchStopHandle?.();
        this.dirtyWatchStopHandle = watch(() => model.$isDirty, debouncedSave);
      },
      { immediate: true }
    );
  }

  stop() {
    this.modelWatchStopHandle?.();
    this.dirtyWatchStopHandle?.();
  }

  useDefaultLifecycle() {
    onMounted(() => this.start());
    onUnmounted(() => this.stop());
    return this;
  }
}
