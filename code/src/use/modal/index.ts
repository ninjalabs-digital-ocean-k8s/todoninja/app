import { pascalize } from "@/inflector";
import { ComponentOptionsWithObjectProps, reactive, watch } from "vue";

import * as ModalComponentsImported from "./components";
export const ModalComponents = ModalComponentsImported;

export class Modal {
  public static inlined() {
    const i = new this(inlined() as any);
    i.inlined = true;
    return i;
  }

  public inlined: boolean = false;

  public state = reactive({
    show: false,
    position: <{ x?: number; y?: number }>{
      x: 0,
      y: 0
    }
  });

  constructor(public component: ComponentOptionsWithObjectProps) {}

  componentWithDefault() {
    return withDefaultModal(this.component, this);
  }

  public show($event: { clientX?: number; clientY?: number } = {}) {
    return new Promise<void>(resolve => {
      this.state.position.x = $event.clientX;
      this.state.position.y = $event.clientY;
      this.state.show = true;

      const unwatcher = watch(
        () => this.state.show,
        () => {
          unwatcher();
          resolve();
        }
      );
    });
  }

  public hide() {
    this.state.show = false;
  }

  public toggle(event?: { clientX?: number; clientY?: number }) {
    if (this.state.show) {
      this.hide();
    } else {
      this.show(event);
    }
  }
}

export const withDefaultModal = (component: any, modal: any) => ({
  ...component,
  props: {
    ...component.props,
    modal: {
      ...component.props.modal,
      required: false,
      default: () => modal
    }
  }
});

export class Modals extends Map<string, Modal> {
  show(name: string, event?: { clientX: number; clientY: number }) {
    return this.get(name)?.show(event);
  }

  hide(name: string) {
    return this.get(name)?.hide();
  }

  get(name: string) {
    return super.get(pascalize(name));
  }

  set(name: string, value: Modal) {
    return super.set(pascalize(name), value);
  }

  inlined(name: string) {
    if (!this.get(name)) {
      this.set(name, Modal.inlined());
    }
    return this.get(name);
  }
}

export const inlined = () => ModalComponents.Frame;

export const useModals = (components: any) => {
  const modals = new Modals();

  for (const name in components) {
    modals.set(name, new Modal(components[name]));
  }

  return modals;
};

// export const hasModals = (components: T) =>
//   Object.assign(
//     () => {
//       const modals = new Modals();

//       for (const name in components) {
//         modals.set(name, new Modal(components[name]));
//       }

//       return modals;
//     },
//     { components }
//   );

export function isModal() {
  return {
    components: { ...ModalComponents },
    props: {
      modal: {
        type: Modal,
        required: true
      }
    }
  };
}
