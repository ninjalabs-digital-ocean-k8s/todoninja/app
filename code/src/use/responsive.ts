import { computed, ref, reactive } from "vue";
import tailwindResolved from "../tailwind.resolved";

export enum Device {
  Mobile = parseInt(tailwindResolved.theme.screens.lg.split("px")[0]),
  Desktop = Infinity
}

export const breakpoints = Object.keys(tailwindResolved.theme.screens).reduce(
  (all, current) => ({ ...all, [current]: tailwindResolved.theme.screens[current].split("px")[0] }),
  {}
);

export const width = ref(0);
const updateWidth = () => (width.value = window.innerWidth > 0 ? window.innerWidth : screen.width);
updateWidth();
window.addEventListener("resize", updateWidth);

export const height = ref(0);
const updateHeight = () => (height.value = window.innerHeight > 0 ? window.innerHeight : screen.height);
updateHeight();
window.addEventListener("resize", updateHeight);

export const currentDevice = computed(() => {
  if (width.value <= Device.Mobile) {
    return Device.Mobile;
  } else {
    return Device.Desktop;
  }
});

export const touchAvailable = "createTouch" in window.document;

export const isMobile = computed(() => currentDevice.value == Device.Mobile);
export const isDesktop = computed(() => currentDevice.value == Device.Desktop);

export const responsive = reactive({
  currentDevice,
  isMobile,
  isDesktop,
  width,
  height,
  Device,
  touchAvailable,
  breakpoints
});

export default responsive;
