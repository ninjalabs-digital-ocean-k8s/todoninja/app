import { reactive } from "@vue/reactivity";

export class Loader {
  _loading: Set<Promise<unknown>> = reactive(new Set());
  _loadingLong: Set<Promise<unknown>> = reactive(new Set());

  constructor(public longTime: number = 500) {}

  get loading() {
    return this._loading.size > 0;
  }
  get loadingLong() {
    return this._loadingLong.size > 0;
  }

  load<T extends Promise<unknown>>(promise: () => T): T;
  load<T extends Promise<unknown>>(promise: T): T;
  load<T extends Promise<unknown>>(_promise: T | (() => T)): T {
    const promise = typeof _promise == "function" ? _promise() : _promise;
    this._loading.add(promise);
    const longTimeout = setTimeout(() => this._loadingLong.add(promise), this.longTime);
    promise.finally(() => {
      clearTimeout(longTimeout);
      this._loading.delete(promise);
      this._loadingLong.delete(promise);
    });
    return promise;
  }
}
