import Setting from "@/models/Setting";
import Task from "@/models/Task";
import Workspace from "@/models/Workspace";
import router from "@/router";
import { computed } from "vue";
import { RouteLocationRaw } from "vue-router";
import { isMobile } from "./responsive";

export const openTask = (id: string) => {
  const route = router.currentRoute.value;
  router[isMobile && currentTaskId.value == undefined ? "push" : "replace"]({
    ...route,
    query: {
      ...route.query,
      task: id
    }
  });
};

export const closeTask = () => {
  const route = router.currentRoute.value;
  router.replace({
    ...route,
    query: {
      ...route.query,
      task: undefined
    }
  });
};

export const currentTaskId = computed(() => {
  return router.currentRoute.value.query?.task;
});

export const openView = (route: RouteLocationRaw, strategy: "replace" | "push" = "replace") => {
  Setting.set("lastView", route);
  return router[strategy](route);
};

export const openWorkspaceFromTask = (task: Task) =>
  task.workspace().execSync().isInbox
    ? openView({ name: "Inbox" }, "push")
    : openView({ name: "Tasks", params: { workspace: task.workspaceId! } }, "push");

export const openWorkspace = (workspace: Workspace) =>
  workspace.isInbox
    ? openView({ name: "Inbox" }, "push")
    : openView({ name: "Tasks", params: { workspace: workspace.id! } }, "push");
