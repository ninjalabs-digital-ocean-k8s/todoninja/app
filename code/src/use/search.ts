import Task from "@/models/Task";
import { computed } from "vue";
import Fuse from "fuse.js";
import { OpaqueModel } from "@opaquejs/opaque";

export type Searchable = typeof searchModels extends (infer U)[]
  ? U extends { model: typeof OpaqueModel }
    ? U["model"]
    : never
  : never;
export type SearchResult = Fuse.FuseResult<InstanceType<Searchable>>;

export const searchModels = [
  {
    model: Task,
    options: {
      keys: [
        "title",
        "description",
        {
          name: "workspaceSync.name",
          weight: 0.1
        }
      ]
    }
  }
];

const fuses = new Map(
  searchModels.map(({ model, options }) => [
    model,
    computed(
      () =>
        new Fuse(model.query().getSync(), {
          shouldSort: true,
          includeScore: true,
          ...options
        })
    )
  ])
);

export const searchAll: (query: string) => SearchResult[] = (query: string) => {
  return [...fuses.values()].map(fuse => fuse.value.search(query)).flat();
};

export const handleResultClick = ({ item }: SearchResult) => {};
