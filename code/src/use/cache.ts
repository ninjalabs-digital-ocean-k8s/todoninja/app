import { reactive } from "@vue/reactivity";

class CacheMap extends Map {
  get<T>(key: unknown, resolver?: () => T): T {
    if (!resolver) {
      return super.get(key);
    }
    if (!this.has(key)) {
      this.set(key, resolver());
    }
    return super.get(key);
  }
}
class AsyncCacheMap extends Map {
  make<T>(key: unknown): unknown;
  make<T>(key: unknown, resolver: () => Promise<T>): undefined | T;
  make<T>(key: unknown, resolver?: () => Promise<T>): T {
    if (resolver && !this.has(key)) {
      this.set(key, reactive({ promise: resolver(), result: undefined }));
      resolver().then(result => this.set(key, { result }));
    }
    return this.get(key).result;
  }
}

export const asyncCache = reactive(new AsyncCacheMap());
export const cache = reactive(new CacheMap());

// const cache_map = new Map<unknown, unknown>();

// export const cache = (identifier: unknown, resolver: () => unknown) => {
//   if (cache_map.has(identifier)) {
//     return cache_map.get(identifier);
//   }
//   if (!scopes.has(id)) {
//     scopes.add(id);
//     query.get();
//     Task.adapter.realtimeSync(query.$getQuery());
//   }
// };
