import { Ref, ref } from "@vue/reactivity";
import { watch, watchEffect, WatchSource } from "@vue/runtime-core";

export const watchedRef = <T>(resolver: () => T, watchExpression?: WatchSource): Ref<T> => {
  let value = ref();
  if (watchExpression) {
    watch(watchExpression, () => (value.value = resolver()), { immediate: true });
  } else {
    watchEffect(() => (value.value = resolver()));
  }
  return value;
};
