import Task from "@/models/Task";
import { DateTime } from "luxon";

export const usePersistentActions = (task: Task) => {
  return {
    clearTimings: () => {
      task.$setAndSaveAttributes(
        {
          postponedUntil: null,
          deadlineAt: null
        },
        { create: false }
      );
    },
    postpone: {
      add: (value = DateTime.local()) =>
        task.$setAndSaveAttributes(
          {
            postponedUntil: value
          },
          { create: false }
        ),
      remove: () =>
        task.$setAndSaveAttributes(
          {
            postponedUntil: null
          },
          { create: false }
        )
    },
    deadline: {
      add: (value = DateTime.local()) =>
        task.$setAndSaveAttributes(
          {
            deadlineAt: value
          },
          { create: false }
        ),
      remove: () =>
        task.$setAndSaveAttributes(
          {
            deadlineAt: null
          },
          { create: false }
        )
    }
  };
};
