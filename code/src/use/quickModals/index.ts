import { reactive, shallowReactive } from "@vue/reactivity";
import { Modal } from "../modal";
import QuickModalVue from "./QuickModal.vue";

export class QuickModals extends Set<Modal> {
  show(value: Modal | QuickModalConfig) {
    const parsed = value instanceof Modal ? value : new QuickModal(value);
    this.add(parsed);
    return parsed.show().then(() => {
      this.delete(parsed);
    });
  }
}

export type QuickModalActionConfig = {
  primary?: boolean;
  color?: string;
  label: string;
  handle: (value: QuickModal) => any;
};
export type QuickModalConfig = {
  title: string;
  description: string;
  color?: string;
  actions: Array<QuickModalAction | QuickModalActionConfig>;
};

export class QuickModal extends Modal {
  constructor(public config: QuickModalConfig) {
    super(QuickModalVue as any);
  }
  get title() {
    return this.config.title;
  }
  get description() {
    return this.config.description;
  }
  get color() {
    return this.config.color;
  }
  get actions() {
    return this.config.actions.map(action => {
      if (action instanceof QuickModalAction) {
        return action;
      }
      return new QuickModalAction(action);
    });
  }
}

export class QuickModalAction {
  static cancel(config?: Partial<QuickModalActionConfig>) {
    return new this({
      label: "cancel",
      handle: modal => modal.hide(),
      ...config
    });
  }
  constructor(public config: QuickModalActionConfig) {}
  get label() {
    return this.config.label;
  }
  get handle() {
    return this.config.handle;
  }
  get primary() {
    return this.config.primary;
  }
  get color() {
    return this.config.color;
  }
}

export const quickModals = shallowReactive(new QuickModals());
