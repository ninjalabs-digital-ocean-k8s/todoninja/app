import { createRouter, createWebHashHistory, RouteLocation, RouteLocationRaw } from "vue-router";
import ListTasks from "@/views/Task/List.vue";
import Today from "@/views/Today.vue";
import Inbox from "@/views/Inbox.vue";
import Upcoming from "@/views/Upcoming.vue";
import Done from "@/views/Done.vue";
import Settings from "@/views/Settings.vue";
import SearchQuery from "@/views/SearchQuery.vue";
import ViewWorkspace from "@/views/Workspaces/View.vue";
import CreateWorkspace from "@/views/Workspaces/Create.vue";
import Login from "@/views/Login.vue";
import Register from "@/views/Register.vue";
import { Auth } from "@/assets/Auth";
import Setting from "@/models/Setting";

const routes: any[] = [
  {
    path: "",
    name: "Home"
  },
  {
    path: "/focus",
    name: "Focus",
    component: Today
  },
  {
    path: "/inbox",
    name: "Inbox",
    component: Inbox
  },
  {
    path: "/upcoming",
    name: "Upcoming",
    component: Upcoming
  },
  {
    path: "/done",
    name: "Done",
    component: Done
  },
  {
    path: "/settings",
    name: "Settings",
    component: Settings
  },
  {
    path: "/search/query/:query?",
    name: "SearchQuery",
    props: (route: RouteLocation) => ({ query: route.params.query }),
    component: SearchQuery
  },
  {
    path: "/workspaces/:workspace",
    props: (route: RouteLocation) => ({ workspaceId: route.params.workspace }),
    name: "EditWorkspace",
    component: ViewWorkspace
  },
  {
    path: "/workspaces/create",
    name: "CreateWorkspace",
    component: CreateWorkspace
  },
  {
    path: "/workspaces/:workspace/tasks",
    name: "Tasks",
    component: ListTasks,
    props: (route: RouteLocation) => ({ workspaceId: route.params.workspace })
  },
  {
    path: "/login",
    name: "Login",
    component: Login
  },
  {
    path: "/register",
    name: "Register",
    component: Register
  }
];

const router = createRouter({
  history: createWebHashHistory(),
  routes
});

const redirectRequest: (to: RouteLocation) => Promise<boolean | RouteLocationRaw> = async to => {
  if (["Login", "Register"].includes(to.name as string)) {
    return (await Auth.check()) ? { name: "Home" } : true;
  } else {
    return (await Auth.check()) ? true : { name: "Login" };
  }
};
const openDefaultView: (to: RouteLocation) => Promise<boolean | RouteLocationRaw> = async to => {
  if (to.name != "Home") {
    return true;
  }
  const setting = await Setting.get("lastView");
  return setting.value || { name: "Inbox" };
};

router.beforeEach(redirectRequest);
router.beforeEach(openDefaultView);

export default router;
